FROM openjdk:21

WORKDIR /app

COPY ./target/paging-mission-control-x-1.0-SNAPSHOT.jar /app
COPY ./sat-com-data /app/sat-com-data

CMD ["java", "-jar", "paging-mission-control-x-1.0-SNAPSHOT.jar"]