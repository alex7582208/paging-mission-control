# Checking Submission

## Using Docker

From terminal, in project root, carry out the following:

**Build Step**  
docker build -t paging-mission-control .

**Run Step**
docker run --name pmc paging-mission-control

**Your result should look similar to this:**  

![img.png](img.png)


## Using IntelliJ
This was made using jdk:21 so you'll need to have that integrated with
IntelliJ.  Once that's setup 
- load maven project
- go to main file in src/main/java/org.example
- there should be a green 'play' button on the main function - click it
- results should appear in the run window

**Your result should look similar to this:**  

![img_1.png](img_1.png)



## Using local version on system
Last option; would recommend only if you want to have multiple versions
running local.  Also recommend using "sdkman!" for this.  The version
management tools have worked great for me in the past.

- ensure your jdk version is 21
- check version in terminal "java --version"
- run this command
  - java -jar ./target/paging-mission-control-x-1.0-SNAPSHOT.jar

**Your result should look similar to this:**  

![img_2.png](img_2.png)