package service_tests;

import org.example.service.ParsedTelemetry;
import org.example.service.TelemetryParser;
import org.example.service.TelemetryTimeframeAnalyzer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.AbstractMap;
import java.util.List;
import java.util.function.Predicate;

public class TelemetryTimeframeAnalyzerTests {
    @Test
    public void testRedHighTempDetection() {
        TelemetryParser telemetryParser = new TelemetryParser("sat-com-data/sat-com-data-003.txt");
        TelemetryTimeframeAnalyzer timeframeAnalyzer = new TelemetryTimeframeAnalyzer();

        try {
            telemetryParser.mapTelemetriesBySatelliteId();
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<ParsedTelemetry> list = telemetryParser.getSatelliteIdMap().get("1000");


        Predicate<ParsedTelemetry> redHighTempCheck = ParsedTelemetry::temperatureIsRedHigh;


        AbstractMap.SimpleImmutableEntry<ParsedTelemetry, Boolean> redHighTempDetected =
                timeframeAnalyzer.checkTelemetriesForStatusCriterion(list, redHighTempCheck);

        Assertions.assertTrue(redHighTempDetected.getValue());
        Assertions.assertEquals("Mon Jan 01 23:06:03 EST 2018", redHighTempDetected.getKey().getTimestamp().toString());
    }

    @Test
    public void testRedLowBattDetection() {
        TelemetryParser telemetryParser = new TelemetryParser("sat-com-data/sat-com-data-004.txt");
        TelemetryTimeframeAnalyzer timeframeAnalyzer = new TelemetryTimeframeAnalyzer();

        try {
            telemetryParser.mapTelemetriesBySatelliteId();
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<ParsedTelemetry> list = telemetryParser.getSatelliteIdMap().get("1000");


        Predicate<ParsedTelemetry> redLowBattCheck = ParsedTelemetry::batteryIsRedLow;


        AbstractMap.SimpleImmutableEntry<ParsedTelemetry, Boolean> redLowBattDetected =
                timeframeAnalyzer.checkTelemetriesForStatusCriterion(list, redLowBattCheck);

        Assertions.assertTrue(redLowBattDetected.getValue());
        Assertions.assertEquals("Mon Jan 01 23:07:07 EST 2018", redLowBattDetected.getKey().getTimestamp().toString());
    }
}
