package service_tests;

import org.example.service.ParsedTelemetry;
import org.example.service.TelemetryParser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

public class TelemetryParserTests {

    @Test
    public void testMapTelemetryListsByKey() {
        TelemetryParser telemetryParser = new TelemetryParser("sat-com-data/sat-com-data-002.txt");

        try {
            telemetryParser.mapTelemetriesBySatelliteId();
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<ParsedTelemetry> _1000 = telemetryParser.getSatelliteIdMap().get("1000");
        List<ParsedTelemetry> _1001 = telemetryParser.getSatelliteIdMap().get("1001");
        List<ParsedTelemetry> _1024 = telemetryParser.getSatelliteIdMap().get("1024");
        List<ParsedTelemetry> _1008 = telemetryParser.getSatelliteIdMap().get("1008");

        // Assert list lengths for individual keys to assert that satellite entities are uniquely identified
        Assertions.assertEquals(7, _1000.size());
        Assertions.assertEquals(7, _1001.size());
        Assertions.assertEquals(15, _1024.size());
        Assertions.assertEquals(10, _1008.size());
    }
}
