package org.example;
import org.example.dto.TelemetryMessageDTO;
import org.example.service.ParsedTelemetry;
import org.example.service.TelemetryParser;
import org.example.service.TelemetryTimeframeAnalyzer;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.AbstractMap.SimpleImmutableEntry;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Paging Mission Control
 * <hr/>
 * <b>Author</b>: Alex Wilson
 * <br/>
 * <b>Date</b>: 21.Feb.2024
 * <br/>
 * <b>Description</b>: Coding Proficiency Assessment using theoretical satellite telemetry
 *  data to create actionable output messages regarding critical satellite conditions.
 * <br/>
 * <b>Notes</b>: one of the most fun coding assessments I've done. I'm a space nerd though so
 *  that probably helped :-)
 */
public class Main {
    public static void main(String[] args) {
        TelemetryParser telemetryParser = new TelemetryParser();
        TelemetryTimeframeAnalyzer timeframeAnalyzer = new TelemetryTimeframeAnalyzer();
        List<TelemetryMessageDTO> outputMessages = new ArrayList<>();
        ObjectMapper outputMapper = new ObjectMapper();

        // Attempt to parse the input data, iterate the logs, and detect noteworthy status conditions.
        try {
            telemetryParser.mapTelemetriesBySatelliteId();
            telemetryParser.getSatelliteIdMap().forEach((key, list) -> {
                // Create and use predicate functions for eval of telemetry status checks
                Predicate<ParsedTelemetry> redLowBattCheck = ParsedTelemetry::batteryIsRedLow;
                Predicate<ParsedTelemetry> redHighTempCheck = ParsedTelemetry::temperatureIsRedHigh;

                // Hold detected status objects for message generation
                SimpleImmutableEntry<ParsedTelemetry, Boolean> redLowBattDetected =
                        timeframeAnalyzer.checkTelemetriesForStatusCriterion(list, redLowBattCheck);
                SimpleImmutableEntry<ParsedTelemetry, Boolean> redHighTempDetected =
                        timeframeAnalyzer.checkTelemetriesForStatusCriterion(list, redHighTempCheck);

                // Create messages for high temp
                if (redHighTempDetected.getValue()) {
                    ParsedTelemetry tempRecord = redHighTempDetected.getKey();
                    outputMessages.add(new TelemetryMessageDTO(tempRecord, "RED HIGH"));
                }

                // Create messages for low battery
                if (redLowBattDetected.getValue()) {
                    ParsedTelemetry battRecord = redLowBattDetected.getKey();
                    outputMessages.add(new TelemetryMessageDTO(battRecord, "RED LOW"));
                }
            });

            // Create JSON string for the final output message
            String jsonMessage = outputMapper.writerWithDefaultPrettyPrinter().writeValueAsString(outputMessages);
            System.out.println(jsonMessage);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error handling bypassed; on authority of the readme!");
        }
    }
}