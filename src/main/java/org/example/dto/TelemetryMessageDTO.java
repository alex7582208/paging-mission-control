package org.example.dto;

import org.example.service.ParsedTelemetry;

import java.time.format.DateTimeFormatter;
//import java

/**
 * Telemetry Message DTO
 * <hr/>
 *
 */
public class TelemetryMessageDTO {
    public final int satelliteId;
    public final String severity;
    public final String component;
    public final String timestamp;

    /**
     * Telemetry Message DTO
     * <hr />
     * create a message DTO to be converted to JSON from telemetry data.
     * @param telemetry parsed telemetry structure / class instance
     * @param newSeverity class name based on threshold of telemetry properties
     */
    public TelemetryMessageDTO(ParsedTelemetry telemetry, String newSeverity) {
        DateTimeFormatter formatter = DateTimeFormatter.ISO_INSTANT;
        String formattedDate = formatter.format(telemetry.getTimestamp().toInstant());

        satelliteId = Integer.parseInt(telemetry.getSatelliteId());
        severity = newSeverity;
        component = telemetry.getComponentName();
        timestamp = formattedDate;
    }
}
