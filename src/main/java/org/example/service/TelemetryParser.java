package org.example.service;
import lombok.Getter;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Telemetry Parser
 * <hr/>
 * text data handling class for satellite telemetry data.  Accomplishes
 * the following:
 * - converts text data to parsed telemetry instance
 * - buffers telemetries into 'concern categories' based on property values
 * - holds the file path of the incoming satellite data
 */
@Getter
public class TelemetryParser {
    String satComDataPath;
    Map<String, List<ParsedTelemetry>> satelliteIdMap = new HashMap<>();

    /**
     * Map Telemetries By Satellite ID
     * <hr/>
     * satellite input data is logged by satellite id, having 4 digits.  The telemetry data contains
     * an identifying number for each satellite, so it makes sense to associate statuses for each satellite individually.
     * after being grouped by 'entity' then the telemetries can be analyzed for status criteria.
     * @throws Exception generic placeholder exception
     */
    public void mapTelemetriesBySatelliteId() throws Exception {
        BufferedReader bReader = new BufferedReader(new FileReader(satComDataPath));

        String nextLine;
        while ((nextLine = bReader.readLine()) != null) {
            ParsedTelemetry pti = parseTelemetryLine(nextLine);

            List<ParsedTelemetry> satelliteTelemetryBuffer = satelliteIdMap.get(pti.getSatelliteId());
            if (satelliteTelemetryBuffer != null) {
                satelliteTelemetryBuffer.add(pti);
            } else {
                List<ParsedTelemetry> newTelemetryBuffer = new ArrayList<>();
                newTelemetryBuffer.add(pti);
                satelliteIdMap.put(pti.getSatelliteId(), newTelemetryBuffer);
            }
        }
    }

    /**
     * Parse Telemetry Line
     * <hr/>
     * accept a string that is pipe delimited and parse that into a class instance
     * with property comparator methods.
     * @param telemetry input string data from satellite
     * @return ParsedTelemetry
     * @throws Exception generic placeholder exception
     */
    public ParsedTelemetry parseTelemetryLine(String telemetry) throws Exception {
        String[] telemetryProps = telemetry.split("\\|");
        return new ParsedTelemetry(telemetryProps);
    }

    /**
     * Telemetry Parser (no arg)
     * <hr/>
     * assign default test data from the project assessment description
     */
    public TelemetryParser() {
        satComDataPath = "sat-com-data/sat-com-data-001.txt";
    }

    /**
     * Telemetry Parser
     * <hr />
     * @param filePath from root path to input telemetry data
     */
    public TelemetryParser(String filePath) {
        satComDataPath = filePath;
    }
}
