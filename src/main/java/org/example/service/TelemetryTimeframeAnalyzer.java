package org.example.service;
import lombok.Getter;
import lombok.Setter;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.time.Duration;
import java.util.function.Predicate;
import java.util.AbstractMap.SimpleImmutableEntry;

/**
 * Telemetry Time Frame Analyzer
 * <hr/>
 * accept a list of parsed telemetries, and check if a specific
 * frequency of 'telemetry type' has occurred within a given
 * window of time.
 */
@Getter
@Setter
public class TelemetryTimeframeAnalyzer {
    private Instant windowStart;
    private Instant windowEnd;
    private int windowInterval;
    private int occurrencesPerWindow;

    /**
     * Window Checker
     * <hr/>
     * determine if the specified number of telemetry conditions have occurred within
     * the target window of time.
     * @param telemetries list of parsed telemetries of a specific condition
     * @param statusCheck predicate function that checks properties of a telemetry for threshold criterion
     * @return boolean
     */
    public SimpleImmutableEntry<ParsedTelemetry, Boolean> checkTelemetriesForStatusCriterion(
            List<ParsedTelemetry> telemetries,
            Predicate<ParsedTelemetry> statusCheck
    ) {
        List<ParsedTelemetry> filteredByStatusCheck = new ArrayList<>();

        // Populate a list with only telemetries that match the concerned status check
        for (ParsedTelemetry parsedTelemetry : telemetries) {
            if (!statusCheck.test(parsedTelemetry)) {
                continue;
            }

            filteredByStatusCheck.add(parsedTelemetry);
        }

        // Don't bother iterating filtered status list if it doesn't contain enough telemetries.
        if (filteredByStatusCheck.size() < occurrencesPerWindow) {
            return new SimpleImmutableEntry<>(telemetries.getFirst(), Boolean.FALSE);
        }

        // check the status of the filtered set against the defined window of occurrences criteria
        int occurrencesAdjusted = occurrencesPerWindow - 1;
        for (int i = 0; i < filteredByStatusCheck.size() - (occurrencesAdjusted); i += 1) {
            windowStart = filteredByStatusCheck.get(i).getTimestamp().toInstant();
            windowEnd = filteredByStatusCheck.get(i + occurrencesAdjusted).getTimestamp().toInstant();
            Duration duration = Duration.between(windowStart, windowEnd);
            if (duration.toMinutes() <= windowInterval) {
                return new SimpleImmutableEntry<>(filteredByStatusCheck.get(i), Boolean.TRUE);
            }
        }

        // some matched conditions over the interval were detected, but not in a high enough frequency
        // together to create a message condition.
        return new SimpleImmutableEntry<>(telemetries.getFirst(), Boolean.FALSE);
    }

    /**
     * Telemetry Time Frame Analyzer (no args)
     * <hr />
     * set default window (time slice) and frequency of occurrence
     */
    public TelemetryTimeframeAnalyzer() {
        this.windowInterval = 5;
        this.occurrencesPerWindow = 3;
    }
}
