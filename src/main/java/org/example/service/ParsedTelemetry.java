package org.example.service;
import lombok.Getter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

@Getter
public class ParsedTelemetry {
    private final Date timestamp;
    private final String satelliteId;
    private final int redHighTempLimit;
    private final int yellowHighTempLimit;
    private final int yellowLowTempLimit;
    private final int redLowTempLimit;
    private final Float processVariableMeasurement;
    private final String componentName;

    /**
     * Battery Is Red Low
     * <hr/>
     * check if the properties of this telemetry indicate a battery reading
     * that is below the red low tolerance threshold.
     * @return boolean
     */
    public boolean batteryIsRedLow() {
        boolean isBatt = Objects.equals(componentName, "BATT");
        boolean isRedLow = processVariableMeasurement < redLowTempLimit;
        return isBatt && isRedLow;
    }

    /**
     * Temperature is Red High
     * <hr/>
     * check if the properties of this telemetry indicate a temperature reading
     * that is above the red high tolerance threshold.
     * @return boolean
     */
    public boolean temperatureIsRedHigh() {
        boolean isTemp = Objects.equals(componentName, "TSTAT");
        boolean isRedHigh = processVariableMeasurement > redHighTempLimit;
        return isTemp && isRedHigh;
    }

    /**
     * Parse Date From Raw Telemetry
     * <hr />
     * create a date object from incoming telemetry so that it's time properties
     * can be compared against a sliding time window for generating alerts.
     * @param rawDateString input date string to be transformed into object
     * @return Date
     * @throws Exception generic placeholder exception
     */
    private Date parseDateFromRawTelemetry(String rawDateString) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
        return formatter.parse(rawDateString);
    }

    /**
     * Parsed Telemetry
     * <hr/>
     * create an interpreted payload of data from the telemetry strings that can be
     * passed around amongst services for analysis, optimized storage, etc.
     * @param rawTelemetryData array of strings to be parsed into corresponding types
     * @throws Exception generic placeholder exception
     */
    public ParsedTelemetry(String[] rawTelemetryData) throws Exception {
        timestamp = parseDateFromRawTelemetry(rawTelemetryData[0]);
        satelliteId = rawTelemetryData[1];
        redHighTempLimit = Integer.parseInt(rawTelemetryData[2]);
        yellowHighTempLimit = Integer.parseInt(rawTelemetryData[3]);
        yellowLowTempLimit = Integer.parseInt(rawTelemetryData[4]);
        redLowTempLimit = Integer.parseInt(rawTelemetryData[5]);
        processVariableMeasurement = Float.parseFloat(rawTelemetryData[6]);
        componentName = rawTelemetryData[7];
    }
}
